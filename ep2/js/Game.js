BasicGame.Game = function(game) {

    //  When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game; //  a reference to the currently running game (Phaser.Game)
    this.add; //  used to add sprites, text, groups, etc (Phaser.GameObjectFactory)
    this.camera; //  a reference to the game camera (Phaser.Camera)
    this.cache; //  the game cache (Phaser.Cache)
    this.input; //  the global input manager. You can access this.input.keyboard, this.input.mouse, as well from it. (Phaser.Input)
    this.load; //  for preloading assets (Phaser.Loader)
    this.math; //  lots of useful common math operations (Phaser.Math)
    this.sound; //  the sound manager - add a sound, play one, set-up markers, etc (Phaser.SoundManager)
    this.stage; //  the game stage (Phaser.Stage)
    this.time; //  the clock (Phaser.Time)
    this.tweens; //  the tween manager (Phaser.TweenManager)
    this.state; //  the state manager (Phaser.StateManager)
    this.world; //  the game world (Phaser.World)
    this.particles; //  the particle manager (Phaser.Particles)
    this.physics; //  the physics manager (Phaser.Physics)
    this.rnd; //  the repeatable random number generator (Phaser.RandomDataGenerator)

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.
};

var player;
var anim;
var poem;
var door;
var pg = new PoemGenerator();
var WALKSPEED = 60;
var FOCALSPEED = 1.2;
var FGSPEED = FOCALSPEED * 1.25;
var BGSPEED = FOCALSPEED * 0.75;
var cameraPos = new Phaser.Point(0, 0);
var allClickable = [];
var selected = null;
var justClicked = null;
var arrows = null;
var props;

BasicGame.Game.prototype = {
    create: function() {
        var bmd;
        var game = this.game;
        var that = this;
        var left = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        var right = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        var up = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        var down = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var gameSound = game.add.audio('bgSound');


        game.sound.setDecodedCallback(gameSound, soundStart, this);

        function soundStart() {
            gameSound.volume = .2;
            gameSound.loopFull();
        }

        var returnNewTexture = function(adjective, object) {
            var innerBitmap = game.make.bitmapData(60, 60);
            innerBitmap.alphaMask(pg.corpus.adjectives[poem[object].id], pg.corpus.objects[poem[adjective].id]);
        return innerBitmap;
        };

    var cycleProperty = function(word, isReversed, dataSet, otherWord) {
        var newTexture;
        if (isReversed) {
                poem[word].id--;
                if (poem[word].id < 0) {
                    poem[word].id = pg.corpus[dataSet].length - 1;
                }
        }
        else {
            poem[word].id++;
                if (poem[word].id > pg.corpus[dataSet].length - 1) {
                    poem[word].id = 0;
                }
            }
        if (dataSet === 'adjectives') {
            newTexture = returnNewTexture(otherWord, word);
            mapChildren(poem[otherWord].frontGrp, function(sprite) {sprite.loadTexture(newTexture);});
            mapChildren(poem[otherWord].backGrp, function(sprite) {sprite.loadTexture(newTexture);});
        }
        else {
            newTexture = returnNewTexture(word, otherWord);
            mapChildren(poem[word].frontGrp, function(sprite) {sprite.loadTexture(newTexture);});
            mapChildren(poem[word].backGrp, function(sprite) {sprite.loadTexture(newTexture);});
        }
        };
    
        var cycle = function(sprite, isReversed, isLeftRightPress) {
            if (selected !== null) {
                switch (sprite.parent) {
                    case poem.firstObj.frontGrp:
                    case poem.firstObj.backGrp:
                        if (isLeftRightPress) {
                            cycleProperty('firstAdj', isReversed, 'adjectives', 'firstObj');
                        } else {
                            cycleProperty('firstObj', isReversed, 'objects', 'firstAdj');
                        }
                        break;
                    case poem.secondObj.frontGrp:
                    case poem.secondObj.backGrp:
                        if (isLeftRightPress) {
                            cycleProperty('secondAdj', isReversed, 'adjectives', 'secondObj');
                        } else {
                            cycleProperty('secondObj', isReversed, 'objects', 'secondAdj');
                        }
                        break;
                    case poem.thirdObj.frontGrp:
                    case poem.thirdObj.backGrp:
                        if (isLeftRightPress) {
                            cycleProperty('thirdAdj', isReversed, 'adjectives', 'thirdObj');
                        } else {
                            cycleProperty('thirdObj', isReversed, 'objects', 'thirdAdj');
                        }
                        break;
                    default:
                        console.log('bug');
                }
            }
        };

        function mapChildren(grp, inFunction) {
            var i = grp.children.length - 1;
            while (i >= 0) {
                if (grp.children) {
                    inFunction(grp.children[i]);
                    i--;
                }
            }
        }

        function setKey(grp, key, value) {
            var i = grp.children.length - 1;

            while (i >= 0) {
                if (grp.children) {
                    grp.children[i][key] = value;
                    i--;
                }
            }
        }

        function clearAll() {
            selected = null;
            arrows.destroy();
            for (var i = allClickable.length - 1; i >= 0; i--) {
                allClickable[i].tint = 0xFFFFFF;
            }
        }

        function createGrps() {
            var y = 90;
            props = [poem.firstObj, poem.secondObj, poem.thirdObj];
            props = props.map(function(prop) {
                prop.backGrp = game.add.group();
                return prop;
            });
            propTextures = [returnNewTexture('firstObj','firstAdj'), 
            returnNewTexture('secondObj','secondAdj'),
            returnNewTexture('thirdObj','thirdAdj')];

            while (y <= 125) {
                props[0].backGrp.create(game.world.randomX*2, y/2, propTextures[0], 0);
                y +=2;
                props[1].backGrp.create(game.world.randomX*2, y/2, propTextures[1], 0);
                y += 2;
                props[2].backGrp.create(game.world.randomX*2, y/2, propTextures[2], 0);
                y += 2;
            }
            player = game.add.sprite(70, 220, 'sprite');
            player.animations.add('walk', [32, 33, 34, 35, 36, 37, 38, 39], 10, true);
            player.anchor.setTo(0.5, 0.5);
            player.scale.y = 2;
            player.scale.x = 2;
            game.physics.p2.enable(player);
            player.body.fixedRotation = true;
            props = props.map(function(prop) {
                prop.frontGrp = game.add.group();
                return prop;
            });

            y += 40;
            while (y <= 300) {
                props[0].frontGrp.create(game.world.randomX*2, y/2, propTextures[0], 0);
                y +=2;
                props[1].frontGrp.create(game.world.randomX*2, y/2, propTextures[1], 0);
                y += 2;
                props[2].frontGrp.create(game.world.randomX*2, y/2, propTextures[2], 0);
                y += 2;
            }

            props = props.map(function(prop) {
                setKey(prop.frontGrp, 'prop', prop);
                setKey(prop.backGrp, 'prop', prop);
                prop.backGrp.setAll('inputEnabled', true);
                prop.frontGrp.setAll('inputEnabled', true);
                prop.frontGrp.setAll('input.pixelPerfectClick', true);
                prop.backGrp.setAll('input.pixelPerfectClick', true);
                prop.frontGrp.scale.set(2, 2);
                prop.backGrp.scale.set(2, 2);
                prop.frontGrp.callAll('events.onInputDown.add', 'events.onInputDown', onSpriteClick);
                prop.backGrp.callAll('events.onInputDown.add', 'events.onInputDown', onSpriteClick);
                allClickable = allClickable.concat(prop.frontGrp.children, prop.backGrp.children);
                return prop;
            });   
        }

        var onSpriteClick = function(inputSprite) {
            clearAll();
            selected = inputSprite;
            console.log("selected sprite x", inputSprite.x);
            arrows = game.add.sprite(-100, -100, 'arrows');
            arrows.x = inputSprite.x * 2;
            arrows.y = inputSprite.y * 2;
            arrows.scale.set(2);
            console.log(props[0].frontGrp.x);
            arrows.x += props[0].frontGrp.x;
        };

        // add callbacks to arrow keys
        left.onDown.add(function() {
            cycle(selected, true, true);
        }, this);
        right.onDown.add(function() {
            cycle(selected, false, true);
        }, this);
        up.onDown.add(function() {
            cycle(selected, false, false);
        }, this);
        down.onDown.add(function() {
            cycle(selected, true, false);
        }, this);

        // generate poem
        poem = pg.generateInitialPoem();

        // turn on features we need
        this.time.advancedTiming = true;

        // set selected to null
        this.physics.startSystem(Phaser.Physics.P2JS);
        this.selectedSprite = null;

        // init keyboard input  
        this.cursors = this.input.keyboard.createCursorKeys();

        // ADD GAME OBJECTS IN THEIR LAYERS
        // MAKE ALL THE GROUPS FIRST

        // BACKGROUND: SKY
        this.bg = this.add.tileSprite(0, 0, game.width, game.height, 'bg1');
        this.bg.fixedToCamera = true;
        this.bg.scale.set(2);

        // MOUNTAINS IN BACKGROUND
        this.bg2 = this.add.tileSprite(0, 0, game.width, game.height, 'mountains');
        this.bg2.fixedToCamera = true;
        this.bg2.scale.set(2);

        // GRASS
        this.mg = this.add.tileSprite(0, 0, game.width, game.height, 'mg1');
        this.mg.fixedToCamera = true;
        this.mg.scale.set(2);

        // INVISIBLE LAYER, I'M SO SORRY, THERE WAS NO OTHER WAY
        clear = this.add.sprite(0, 0, 'clear');
        clear.inputEnabled = true;
        clear.events.onInputDown.add(clearAll, this);

        // ON GRASS BEHIND PLAYER
        door = game.add.sprite(940*3, 154, 'door');
        door.scale.set(2);
        createGrps();

        // ON GRASS IN FRONT OF PLAYER

        var t1, t2, t3;
        // TEXT
        text = game.add.text(32, 32, 'blah', {
            font: "20pt Courier",
            fill: "#fff",
            backgroundColor: 'rgb(0,0,0)'
        });
        text2 = game.add.text(32, 64, 'text2', {
            font: "20pt Courier",
            fill: "#fff",
            backgroundColor: 'rgb(0,0,0)'
        });
        text3 = game.add.text(32, 96, 'text3', {
            font: '20pt Courier',
            fill: "#fff",
            backgroundColor: 'rgb(0,0,0)'
        });
        cameraPos.setTo(player.x, player.y);

        // ARROWS
        arrows = this.add.sprite(-100, -100, 'arrows');
        arrows.scale.set(2);
    },

    update: function() {
        player.body.setZeroVelocity();
        var lines = pg.assemble(pg.poemFromNums(poem));
        lines[0] = lines[0].join(' ');
        lines[1] = lines[1].join(' ');
        lines[2] = lines[2].join(' ');
        if (selected === null) {
            if (this.cursors.left.isDown && (player.x > 32)) {
                player.animations.play('walk');
                player.scale.x = -2;
                player.body.moveLeft(WALKSPEED);
                this.bg2.tilePosition.x += BGSPEED;
                this.mg.tilePosition.x += FOCALSPEED;
                props.map(function(prop) {
                    prop.frontGrp.x += FOCALSPEED*2;
                    prop.backGrp.x += FOCALSPEED*2;
                });
                door.x += FOCALSPEED*2;
            } else if (this.cursors.right.isDown && (player.x < 885)) {
                player.animations.play('walk');
                player.scale.x = 2;
                player.body.moveRight(WALKSPEED);
                // this.fg.tilePosition.x -= FGSPEED;
                    this.bg2.tilePosition.x -= BGSPEED;
                    this.mg.tilePosition.x -= FOCALSPEED;
                    door.x -= FOCALSPEED*2;
                    //mgsprites.x -= FOCALSPEED*2;
                    props.map(function(prop) {
                        prop.frontGrp.x -= FOCALSPEED*2;
                        prop.backGrp.x -= FOCALSPEED*2;
                });
            }
            else if (this.cursors.right.isDown && (player.x > 885)) {
                inject(lines.join("\n"));
            } else {
                player.animations.stop();
                player.frame = 64;
            }
        }

        // update text
        text.setText(lines[0]);
        text2.setText(lines[1]);
        text3.setText(lines[2]);
    },

    quitGame: function(pointer) {
        //  Here you should destroy anything you no longer need.
    }
};
