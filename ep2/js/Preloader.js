
BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function () {

		//	These are the assets we loaded in Boot.js
		//	A nice sparkly background and a loading progress bar
//		this.background = this.add.sprite(0, 0, 'preloaderBackground');
//		this.preloadBar = this.add.sprite(300, 400, 'preloaderBar');

		//	This sets the preloadBar sprite as a loader sprite.
		//	What that does is automatically crop the sprite from 0 to full-width
		//	as the files below are loaded in.
//		this.load.setPreloadSprite(this.preloadBar);

		//	Here we load the rest of the assets our game needs.
		//	As this is just a Project Template I've not provided these assets, swap them for your own.
//		this.load.image('titlepage', 'images/title.jpg');
//		this.load.atlas('playButton', 'images/play_button.png', 'images/play_button.json');
//		this.load.audio('titleMusic', ['audio/main_menu.mp3']);
//		this.load.bitmapFont('caslon', 'fonts/caslon.png', 'fonts/caslon.xml');
		//	+ lots of other required assets here
		this.load.audio('bgSound', 'assets/crickets.ogg');
	    this.load.spritesheet('sprite', 'assets/sprite.png', 32, 32, 72);
	    this.load.image('clear', 'assets/clear.png');
  	    this.load.image('fg1', 'assets/fg1.gif');
	    this.load.image('mountains', 'assets/photomountains2.png', 384, 192);
	    this.load.image('bg1', 'assets/bg1.gif');
	    this.load.image('mask', 'assets/mask.png')
	    this.load.spritesheet('mg1', 'assets/mg1.png', 192, 192);
	    this.load.spritesheet('uglysprite', 'assets/uglysprite.png', 40, 40);
	    this.load.image('arrows', 'assets/arrows.gif');
	    this.load.image('door', 'assets/door.png');

	    // backgrounds
	    this.load.image('rose', 'assets/rosy.png');
	    this.load.image('dusty', 'assets/dusty.png');
	    this.load.image('empty', 'assets/empty.png');
	    this.load.image('tall', 'assets/tall.png');
	    this.load.image('plum', 'assets/plum.png');
	    this.load.image('long', 'assets/long.png');
	    this.load.image('sharp', 'assets/sharp.png');
	    this.load.image('white', 'assets/white.png');
	    this.load.image('black', 'assets/black.png');

	    // silhouettes
	    this.load.image('pines', 'assets/pines.png');
	    this.load.image('trees', 'assets/trees.png');
	    this.load.image('ponds', 'assets/ponds.png');
	    this.load.image('lamps', 'assets/lamps.png');
	    this.load.image('moons', 'assets/moons.png');
	    this.load.image('kites', 'assets/kites.png');
	    this.load.image('petals', 'assets/petals.png');
	    this.load.image('sparrows', 'assets/sparrows.png');
	    this.load.image('boats', 'assets/boats.png')
	    this.load.image('dragonflies', 'assets/dragonflies.png');
	},

	create: function () {

		//	Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
//		this.preloadBar.cropEnabled = false;

	},

	update: function () {

		//	You don't actually need to do this, but I find it gives a much smoother game experience.
		//	Basically it will wait for our audio file to be decoded before proceeding to the MainMenu.
		//	You can jump right into the menu if you want and still play the music, but you'll have a few
		//	seconds of delay while the mp3 decodes - so if you need your music to be in-sync with your menu
		//	it's best to wait for it to decode here first, then carry on.
		
		//	If you don't have any music in your game then put the game.state.start line into the create function and delete
		//	the update function completely.
		
//		if (this.cache.isSoundDecoded('titleMusic') && this.ready == false)
//		{
			this.ready = true;
			this.state.start('Game');
//		}

	}

};
