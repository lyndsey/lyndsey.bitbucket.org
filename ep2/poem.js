var pickRandom = function(items) {
    return items[Math.floor(Math.random()*items.length)];
}

function PoemGenerator() {
    var that = this;
    this.corpus =
	{ prepositions: ["amid", "amongst", "above", "beyond"],
	  singleAdjectives: ["long", "tall", "sharp", "white", "plum", "black", "red", "green", "rose", "sharp"],
	  doubleAdjectives: ["little", "hazy", "dancing", "dewy","dusty", "noontime", "warming"],
	  tripleAdjectives: ["shimmering"],
	  feels: ["scent", "sound", "voice", "taste", "tide", "death", "warmth", "chill", "glow"],
	  singleObjects: ["trees", "ponds", "pines", "lamps", "hills", "moons", "kites"],
	  doubleObjects: ["moonbeams", "petals", "swallows"],
	  tripleObjects: ["dragonflies"],
	  verbs: ["kisses", "cradles", "flows through", "touches", "obscures", "brightens", "darkens"]
	};
    this.corpus.adjectives = ['dusty', 'rose', 'empty','tall','plum','long','sharp','white','black']; //this.corpus.singleAdjectives.concat(this.corpus.doubleAdjectives, this.corpus.tripleAdjectives);
    this.corpus.objects = ['trees', 'pines', 'ponds', 'dragonflies', 'lamps', 'moons', 'kites', 'petals',
    					   'sparrows', 'boats']; //this.corpus.singleObjects.concat(this.corpus.doubleObjects, this.corpus.tripleObjects);
    this.pickRandom = function(items) {
	return Math.floor(Math.random()*items.length);
    };
    this.composeLine = function(array) {
	return;
    };	
    this.composePoem = function() {
	return;
    };
    this.assemble = function(poem) {
	return [
	    [poem.preposition, poem.firstAdj, poem.firstObj],
	    ["the", poem.feels, "of", poem.secondAdj, poem.secondObj],
	    [poem.verb, poem.thirdAdj, poem.thirdObj]
	];
    }
    this.poemFromNums = function(poemObj) {
	return {
	    preposition: that.corpus.prepositions[poemObj.preposition.id],
	    firstAdj: that.corpus.adjectives[poemObj.firstAdj.id],
	    firstObj: that.corpus.objects[poemObj.firstObj.id],
	    feels: that.corpus.feels[poemObj.feels.id],
	    secondAdj: that.corpus.adjectives[poemObj.secondAdj.id],
	    secondObj: that.corpus.objects[poemObj.secondObj.id],
	    verb: that.corpus.verbs[poemObj.verb.id],
	    thirdAdj: that.corpus.adjectives[poemObj.thirdAdj.id],
	    thirdObj: that.corpus.objects[poemObj.thirdObj.id]
	};
    },
    this.rand = function(input) {
	return that.pickRandom(pg.corpus[input]);
    },
    this.generateInitialPoem = function() {
	return {
	    preposition: {id: that.rand("prepositions")},
	    firstAdj: {id: that.rand("adjectives")},
	    firstObj: {id: that.rand("objects")},
	    feels: {id: that.rand("feels")},
	    secondAdj: {id: that.rand("adjectives")},
	    secondObj: {id: that.rand("objects")},
	    verb: {id: that.rand("verbs")},
	    thirdAdj: {id: that.rand("adjectives")},
	    thirdObj: {id: that.rand("objects")}
	};
    }
}

var pg = new PoemGenerator();

var fakePoem2 = {
    preposition: 1,
    firstAdj: 2,
    firstObj: 4,
    feels: 3,
    secondAdj: 5, 
    secondObj: 2,
    verb: 4,
    thirdAdj: 9, 
    thirdObj: 0 
};

//console.log(fakePoemObject);
//console.log(pg.assemble(pg.poemFromNums(fakePoem2)));    
		
    
