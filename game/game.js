var WIDTH = 896;
var HEIGHT = 512;
var HZMIN = 1100;
var HZMAX = 2100;
var currentMap = 0;

console.log("whee");

var game = new Phaser.Game(WIDTH, HEIGHT, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render });

game.state.add("GameOver",gameOver);

function preload() {
    
    game.load.tilemap('map0', 'assets/collision_test3_2.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map1', 'assets/level2_2.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map2', 'assets/level3_3.json', null, Phaser.Tilemap.TILED_JSON);
   game.load.tilemap('map3', 'assets/level4_4.json', null, Phaser.Tilemap.TILED_JSON);
 //   game.load.tilemap('map4', 'assets/level4_4.json', null, Phaser.Tilemap.TILED_JSON); 
/*    game.load.tilemap('map6', 'assets/level6.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map7', 'assets/level7.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map7', 'assets/level8.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map9', 'assets/level9.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map10', 'assets/level10.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map11', 'assets/level11.json', null, Phaser.Tilemap.TILED_JSON); */
    game.load.image('tilemap', 'assets/tilemap.png');
    game.load.image("background1", "assets/background1.jpg"); 
    game.load.image("background2", "assets/background2.jpg");
    game.load.image("background3", "assets/background3.jpg");
    game.load.image("background4", "assets/background4.jpg");
    game.load.image("background5", "assets/background5.jpg");
    game.load.spritesheet('bird', 'assets/bird32.png', 32, 32, 5);
}

var map;
var layer;
var cursors;
var sprite;
var score = 0;
var timeText;
var maps = [];

function endGame() {
    score+= sprite.position.x;
    this.game.state.start("GameOver",true,false,score);
};

function refreshSprite() {
    sprite = game.add.sprite(0, 256, 'bird');
    sprite.animations.add('fly');
    sprite.animations.play('fly', 7, true);
    sprite.anchor.setTo(.5, 1); 
    game.physics.enable(sprite);
    
    //    sprite.body.bounce.set(0.6);
    sprite.body.tilePadding.set(32);    
    sprite.body.collideWorldBounds = true;
    game.camera.follow(sprite);
    cursors = game.input.keyboard.createCursorKeys();	
}

function create() {
    
    var angle = { min: 0, max: 0 };
    
    this.add.tween(angle).to( { max: 360 }, 600000, "Linear", true, 0, -1, true);
    
    game.time.events.add(Phaser.Timer.SECOND * 60, endGame, this);
    
    background = game.add.tileSprite(0, 0, 1792, 512, "background1");
    var mapCount = 4;

    for (var i = 0; i < mapCount; ++i) {
	maps[i] = game.add.tilemap('map' + i);
	maps[i].addTilesetImage('tilemap');
	maps[i].setCollision(0);
	maps[i].setCollision(1);
	maps[i].setCollision(3);
	maps[i].setCollision(4);
	maps[i].setCollision(5);
	maps[i].setCollision(6);
	maps[i].setCollision(7);
	maps[i].setCollision(8);
	maps[i].setCollision(10);
	maps[i].setCollision(11);
	maps[i].setCollision(12);
	maps[i].setCollision(13);
	maps[i].setCollision(14);
	maps[i].setCollision(15);
	maps[i].setTileIndexCallback(9, function() {
	    nextLevel();
	}, this);
	graphics2 = game.add.graphics(0, 0);
	
    }
       
    layer = maps[currentMap].createLayer('Tile Layer 1');

    layer.resizeWorld();

    game.world.setBounds(0, 0, game.width, game.height);
    
    refreshSprite();
}

function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function nextLevel() {
    console.log("start of nextlevel");
    score+=512;
    if (sprite.position.x == 0) {
	console.log("returning early");
	return;
	}
    currentMap++;
    console.log("drawing bg");
    background = game.add.tileSprite(0, 0, 1024, 512, "background" + getRandomArbitrary(1,5));
    console.log(currentMap);
    console.log("map");
    console.log(maps[currentMap]);
    sprite.position.x = 0;
    layer.destroy();
    console.log("destroying layer and drawing new one");
    layer = maps[currentMap].createLayer('Tile Layer 1');
    layer.resizeWorld();
    var y = sprite.position.y;
    refreshSprite();
    sprite.position.y = y;
    console.log("exiting nextlevel");
}

function getTargetY(hz) {
    if (hz > 0) {
	return HEIGHT - (((hz - 1100) * (512 - 0)) / (2100 - 1100));
    }
    else {
	return (HEIGHT/2);
    }
}

function update() {
var angle = { min: 0, max: 0 };
game.debug.text("Time: " + game.time.events.duration, 10, 20);
/*    var timerText = this.game.add.text();
    timeText = "Time: " + game.time.events.duration;
    timerText.destroy();
    timerText = this.game.add.text(game.world.centerX-440, 0, timeText, style); */
    game.physics.arcade.collide(sprite, layer);

graphics2.clear();
    graphics2.lineStyle(0, 0xffffff);
    graphics2.beginFill(0xffffff);
    graphics2.arc(600, 300, 160, angle.min, game.math.degToRad(angle.max), true);
    graphics2.endFill();
    
  //  console.log(currentFreq);
    sprite.targetY = getTargetY(currentFreq);
    //  Un-comment these to gain full control over the sprite
    // sprite.body.velocity.x = 0;
    // sprite.body.velocity.y = 0;

    //debug no whistle
/*    if (cursors.up.isDown) {
        sprite.targetY = 512;
    }
    else {
	sprite.targetY = 0;
    }
  */  
    sprite.body.velocity.x = 150;

    if ((sprite.position.y <= sprite.targetY + 10) &&
	(sprite.position.y >= sprite.targetY - 10))
    {
	sprite.body.velocity.y = 0;
    }
    else {
	if (sprite.position.y > sprite.targetY)
	{
            sprite.body.velocity.y = -150;
	}
	else
	{
            sprite.body.velocity.y = 150;
	}
    }
}

function render() {
    // game.debug.spriteBounds(sprite);
    // game.debug.cameraInfo(game.camera, 32, 32);
    // game.debug.body(sprite);
//    game.debug.bodyInfo(sprite, 32, 32);

}
