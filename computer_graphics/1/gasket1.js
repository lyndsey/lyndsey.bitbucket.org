var gl;
var points = [];
var updateDisplay;

// change these variables for different results
var numTimesToSubdivide = 5;
var angle=-.5;

function draw(draw) {
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
    alert("WebGL isn't available");
    }

    // First, initialize the corners
    var vertices = [
    vec2(-.65, -.65),
    vec2(0, .65),
    vec2(.65, -.65)
    ];
    
    // draw a triangle
    function triangle(a, b, c) {
    points.push(a, b, c);
    }
    
    function divideTriangle(a, b, c, count) {
    if (count === 0) {
        triangle(a, b, c);
    }
    else {
        // bisectng sides
        // it was like this in examples so I left it even
        // though I'm sure douglas crockford would cry
        var ab = mix(a, b, 0.5);
        var ac = mix(a, c, 0.5);
        var bc = mix(b, c, 0.5);
        --count;
        // four new triangles
        divideTriangle(a, ab, ac, count);
        divideTriangle(c, ac, bc, count);
        divideTriangle(b, bc, ab, count);
        divideTriangle(ab, ac, bc, count);
    }
    }

    function getDistanceFromOrigin(x, y) {
    return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    function getTwistedX(x, y, angle, d) {
    return (x*Math.cos(angle*d)) - (y*Math.sin(angle*d));
    }

    function getTwistedY(x, y, angle, d) {
    return (x*Math.sin(angle*d)) + (y*Math.cos(angle*d));
    }
    
    function twist(vertex) {
    var newVertex = [];
    var d = getDistanceFromOrigin(vertex[0], vertex[1]);
    newVertex[0] = getTwistedX(vertex[0], vertex[1], angle, d);
    newVertex[1] = getTwistedY(vertex[0], vertex[1], angle, d);
    return newVertex;
    }
    
    divideTriangle(vertices[0], vertices[1], vertices[2], numTimesToSubdivide);
    points = points.map(twist);
    
    //  Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    //  Load shaders and initialize attribute buffers

    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Load the data into the GPU~

    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW);

    // Associate out shader variables with our data buffer

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    render();
}

window.onload = function init() {
// html elements
    var canvas = document.getElementById("gl-canvas");
    var depthDisplay = document.getElementById("depth");
    var angleDisplay = document.getElementById("angle");
    var depthPlus = document.getElementById("depth-plus");
    var depthMinus = document.getElementById("depth-minus");
    var anglePlus = document.getElementById("angle-plus");
    var angleMinus = document.getElementById("angle-minus");

    depthPlus.onclick = function() {
	numTimesToSubdivide += 1;
	draw(canvas);
    }

    depthMinus.onclick = function() {
	numTimesToSubdivide -= 1;
	draw(canvas);
    }

    anglePlus.onclick = function() {
	angle += .1;
	draw(canvas);
    }

    angleMinus.onclock = function() {
	angle -= .1;
	draw(canvas);
    }
    
    updateDisplay = function updateDisplay() {
	console.log(depthDisplay, angleDisplay);
	depthDisplay.innerHTML = numTimesToSubdivide;
	angleDisplay.innerHTML = angle;
    }
};

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, points.length);
    updateDisplay();
}
